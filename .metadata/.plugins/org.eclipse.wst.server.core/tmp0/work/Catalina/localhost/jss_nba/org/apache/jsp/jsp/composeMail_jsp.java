/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: Apache Tomcat/7.0.53
 * Generated at: 2016-03-13 08:27:19 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.*;
import com.dbutil.*;
import com.can.*;

public final class composeMail_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
        throws java.io.IOException, javax.servlet.ServletException {

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			"/jss_nba/jsp/errorPage.jsp", true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("     \r\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">\r\n");
      out.write("<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,500' rel='stylesheet' type='text/css'> \r\n");
      out.write("\t\t<link href='http://fonts.googleapis.com/css?family=Lato:300,400' rel='stylesheet' type='text/css'> \r\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"/jss_nba/bootstrap/inc/bootstrap/css/bootstrap.min.css\">\r\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"/jss_nba/bootstrap/inc/animations/css/animate.min.css\">\r\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"/jss_nba/bootstrap/inc/font-awesome/css/font-awesome.min.css\"> <!-- Font Icons -->\r\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"/jss_nba/bootstrap/inc/owl-carousel/css/owl.carousel.css\">\r\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"/jss_nba/bootstrap/inc/owl-carousel/css/owl.theme.css\">\r\n");
      out.write("\r\n");
      out.write("\t\t<!-- Theme CSS -->\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"/jss_nba/bootstrap/css/reset.css\">\r\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"/jss_nba/bootstrap/css/style.css\">\r\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"/jss_nba/bootstrap/css/mobile.css\">\r\n");
      out.write("\r\n");
      out.write("\t\t<!-- Skin CSS -->\r\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"/jss_nba/bootstrap/css/skin/cool-gray.css\">\r\n");
      out.write("\t\t<header id=\"header\" class=\"header-main\">\r\n");
      out.write("\r\n");
      out.write("             \r\n");
      out.write("                <!-- Begin Navbar -->\r\n");
      out.write("                <nav id=\"main-navbar\" class=\"navbar navbar-default navbar-fixed-top\" role=\"navigation\"> <!-- Classes: navbar-default, navbar-inverse, navbar-fixed-top, navbar-fixed-bottom, navbar-transparent. Note: If you use non-transparent navbar, set \"height: 98px;\" to #header -->\r\n");
      out.write("\r\n");
      out.write("                  <div class=\"container\">\r\n");
      out.write("\r\n");
      out.write("                    <!-- Brand and toggle get grouped for better mobile display -->\r\n");
      out.write("                    <div class=\"navbar-header\">\r\n");
      out.write("                      <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">\r\n");
      out.write("                        <span class=\"sr-only\">Toggle navigation</span>\r\n");
      out.write("                        <span class=\"icon-bar\"></span>\r\n");
      out.write("                        <span class=\"icon-bar\"></span>\r\n");
      out.write("                        <span class=\"icon-bar\"></span>\r\n");
      out.write("                      </button>\r\n");
      out.write("                      <a class=\"navbar-brand page-scroll wow zoomIn\" data-wow-delay=\"s\" href=\"#\">JSS-NBA</a>\r\n");
      out.write("                    </div>\r\n");
      out.write("\r\n");
      out.write("                    <!-- Collect the nav links, forms, and other content for toggling -->\r\n");
      out.write("                    <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">\r\n");
      out.write("                        <ul class=\"nav navbar-nav navbar-right\">\r\n");
      out.write("                        <li class=\"dopdown\">\r\n");
      out.write("                        <a href=\"#\" class=\"dropdown-toggle wow zoomIn\" data-toggle=\"dropdown\">Account<b class=\"caret\"></b></a>\r\n");
      out.write("\t\t\t             <ul class=\"dropdown-menu\">\r\n");
      out.write("\t\t\t    <li><a class=\"page-scroll wow zoomIn\"  data-wow-delay=\"0.1s\" href=\"#about-section\">Inbox</a></li>\r\n");
      out.write("\t\t\t    <li><a class=\"page-scroll wow zoomIn\"  data-wow-delay=\"0.2s\" href=\"#about-section\">Sent Box</a></li>\r\n");
      out.write("                <li class=\"devider\"></li>\r\n");
      out.write("                <li><a class=\"page-scroll wow shake\" data-wow-delay=\"0.2s\" href=\"/jss_nba/jsp/LogOut.jsp\">Log Out</a></li>\r\n");
      out.write("                    </ul>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    </ul>\r\n");
      out.write("                    </div><!-- /.navbar-collapse -->\r\n");
      out.write("                  </div><!-- /.container -->\r\n");
      out.write("                </nav>\r\n");
      out.write("                <!-- End Navbar -->\r\n");
      out.write("\r\n");
      out.write("            </header>\r\n");
      out.write("</head>\r\n");
      out.write("<body style=\"background-image: url('/e-canvass/images/10.jpg');background-position:center; background-repeat: no-repeat;height:100%;width:100%;background-attachment: fixed;\">\r\n");
      out.write("<div class=\"container\">\r\n");
HttpSession hs=request.getSession(false);
String userid=(String)hs.getAttribute("loginid");
String ty=(String)hs.getAttribute("type1");
/* String page1=(String)hs.getAttribute("page");
hs.setAttribute("page", page1);
 */
String s=request.getQueryString();
System.out.println("ye hai"+s);
String rec=(String)hs.getAttribute("rec");
System.out.println("sender id is "+userid);
hs.setAttribute("senderid", userid);
System.out.println(ty);
//logged in user id

      out.write("\r\n");
      out.write("<div style=\"position: relative;top:90px;\" class=\"page-header text-center wow zoomIn\">\r\n");
      out.write("<h2>Compose Mail</h2>\r\n");
      out.write("<div class=\"devider\"></div>\r\n");
      out.write("</div>\r\n");
      out.write("<div class=\"form-group\">\r\n");
      out.write("<form action=\"/e-canvass/Mail?");
      out.print(ty );
      out.write("\" method=\"Post\">\r\n");
      out.write("<table style=\"position:relative;  top:40px;\">\r\n");
      out.write("<tr><td><label class=\"wow zoomIn\" data-wow-delay=\"0.8s\" for=\"to\">To:</label>\r\n");
      out.write("<input id=\"to\" style=\"width: 20%;\" class=\"form-control wow bounceInRight\" data-wow-delay=\"0.1s\" type=\"text\" name=\"id\" value=");
      out.print(s);
      out.write("></td></tr>\r\n");
      out.write("<tr><td><label class=\"wow zoomIn\" data-wow-delay=\"0.9s\" for=\"sub\">Subject:</label>\r\n");
      out.write("<input id=\"sub\" style=\"width: 20%;\" class=\"form-control wow bounceInRight\" data-wow-delay=\"0.2s\" type=\"text\" value=\"\" name=\"subject\"></td></tr>\r\n");
      out.write("<tr><td><label class=\"wow zoomIn\" data-wow-delay=\"0.99s\" for=\"mes\">Message:</label>\r\n");
      out.write("<textarea id=\"mes\" class=\"form-control wow bounceInRight\" data-wow-delay=\"0.3s\" name=\"message\" rows=\"10\" cols=\"100\"></textarea></td></tr>\r\n");
      out.write("<tr><td>\r\n");
      out.write("<input type=\"submit\" class=\"btn btn-success wow shake\" data-wow-delay=\"1s\" value=\"Send\">\r\n");
      out.write("</td></tr>\r\n");
      out.write("</table>\r\n");
      out.write("</form>\r\n");
      out.write("</div>\r\n");
      out.write("<script src=\"/jss_nba/bootstrap/inc/jquery/jquery-1.11.1.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"/jss_nba/bootstrap/inc/bootstrap/js/bootstrap.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"/jss_nba/bootstrap/inc/owl-carousel/js/owl.carousel.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"/jss_nba/bootstrap/inc/stellar/js/jquery.stellar.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"/jss_nba/bootstrap/inc/animations/js/wow.min.js\"></script>\r\n");
      out.write("        <script src=\"/jss_nba/bootstrap/inc/isotope.pkgd.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"/jss_nba/bootstrap/js/theme.js\"></script>\r\n");
      out.write("</div></body>\r\n");
      out.write("</html>");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
