package com.can;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dbutil.CRUDOperation;

/**
 * Servlet implementation class adddelsub
 */
@WebServlet("/adddelsub")
public class adddelsub extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Connection con;java.sql.PreparedStatement ps=null,ps1=null,ps2=null;ResultSet rs=null;  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public adddelsub() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession hs=request.getSession(false);
		String arr[]=request.getParameterValues("sumiksha");
		int l=arr.length;
		String strsql="delete from subjects where code=?";
		con=CRUDOperation.createConection();
		try
		{
		//con.setAutoCommit(false);
		for(int i=0;i<l;i++)
		{		
			ps=con.prepareStatement(strsql);
			String table="drop table "+arr[i].toUpperCase();
			String table1="drop table "+arr[i].toUpperCase()+"copo";
			ps1=con.prepareStatement(table);
			ps2=con.prepareStatement(table1);
			ps.setString(1, arr[i]);
			ps.executeUpdate();	
			ps1.executeUpdate();
			ps2.executeUpdate();
		}	
	}
		catch(SQLException se)
		{
			System.out.println(se);
		}
		response.sendRedirect("/jss_nba/jsp/admin.jsp");
	}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession hs=request.getSession(false);
		String code=request.getParameter("cod");
		String branch=request.getParameter("branch");
		String subject=request.getParameter("sub");
		String strsql="insert into subjects values(?,?,?)";
		String strsql1="select * from subjects where code=?";
		String strsql2="create table "+code.toUpperCase()+"(co varchar(10),text longtext)";
		String str="create table "+code.toUpperCase()+"copo(code varchar(10),po1 integer,po2 integer,po3 integer,po4 integer,po5 integer,po6 integer,po7 integer,po8 integer,po9 integer,po10 integer,po11 integer,po12 integer)";
		con=CRUDOperation.createConection();
		try
		{   ps1=con.prepareStatement(strsql1);
		    ps1.setString(1, code);
		    rs=ps1.executeQuery();
		    if(!rs.next())
		    {
			ps=con.prepareStatement(strsql);
			ps2=con.prepareStatement(strsql2);
			ps2.executeUpdate();
			ps.setString(1, code);
			ps.setString(2, subject);
			ps.setString(3, branch);
			ps.executeUpdate();
			ps1=con.prepareStatement(str);
			ps1.executeUpdate();
		    }
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		response.sendRedirect("/jss_nba/jsp/admin.jsp");
	}

}
