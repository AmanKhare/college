package com.can;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dbutil.CRUDOperation;

/**
 * Servlet implementation class addelbranch
 */
@WebServlet("/addelbranch")
public class addelbranch extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Connection con;java.sql.PreparedStatement ps=null,ps1=null,ps2=null;ResultSet rs=null;   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public addelbranch() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession hs=request.getSession(false);
		String tt=request.getParameter("branchtitle");
		String ss="insert into branch values(?)";
		con=CRUDOperation.createConection();
		try
		{
			ps=con.prepareStatement(ss);
			ps.setString(1,tt);
			ps.executeUpdate();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		response.sendRedirect("/jss_nba/jsp/admin.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession hs=request.getSession(false);
		String tt[]=request.getParameterValues("delbranch");
		String ss="delete from branch where branch=?";
		con=CRUDOperation.createConection();
		try
		{
			ps=con.prepareStatement(ss);
			for(int i=0;i<tt.length;i++)
			{
			String ss1="drop table "+tt[i].toLowerCase()+"pattern";
			ps1=con.prepareStatement(ss1);
			ps.setString(1,tt[i]);
			ps.executeUpdate();
			ps1.executeUpdate();
		}}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		response.sendRedirect("/jss_nba/jsp/admin.jsp");
	}
}
