package com.can;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dbutil.CRUDOperation;

/**
 * Servlet implementation class delcopo
 */
@WebServlet("/delcopo")
public class delcopo extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Connection con;java.sql.PreparedStatement ps=null,ps1=null,ps2=null;ResultSet rs=null;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public delcopo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession hs=request.getSession(false);
		String s[]=request.getParameterValues("delcopo");
		String ss="delete from pos where title=?";
		con=CRUDOperation.createConection();
		try
		{
			ps=con.prepareStatement(ss);
			for(int i=0;i<s.length;i++)
			{
			ps.setString(1, s[i]);
			ps.executeUpdate();
		}}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		response.sendRedirect("/jss_nba/jsp/admin.jsp");
	}

}
